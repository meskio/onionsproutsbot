#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: Panagiotis "Ivory" Vasilopoulos <git@n0toose.net>
#
# :copyright:   (c) 2020-2022, Panagiotis "Ivory" Vasilopoulos
#
# :license: This is Free Software. See LICENSE for license information.
#

import asyncio
import logging

from aiofiles.tempfile import NamedTemporaryFile
from aiohttp import ClientSession
from pyrogram import Client

'''
It is assumed that all of these functions will only be
executed only after the bot has been initialized.

By "initialized", it is meant that the bot has been
configured with a configuration file and successfully
connected to Telegram's servers.
'''

# TODO: Show a progress bar in a Telegram message instead of using stdout.
async def progress(current, total):
    logging.debug(f"{current / total * 100:.1f}%")

async def relay_files(
    client: Client,
    callback: str,
    url: str,
    original_name: str,
    directory,
    _
) -> str:
    try:
        async with ClientSession(raise_for_status=True) as session:
            async with session.get(url) as response:
                async with NamedTemporaryFile(mode='wb', dir=directory) as file:
                    await file.write(await response.read())
                    await file.seek(0)

                    sent_file = await client.send_document(
                        callback.from_user.id,
                        document = file.name,
                        file_name = original_name,
                        progress = progress
                    )

                    # Returns file ID from Telegram.
                    # It will be stored in a local database later.
                    return sent_file["document"]["file_id"]
    except Exception as e:
        logging.exception(e)
        await client.send_message(
            callback.from_user.id,
            _("Upload failed! Please try again later. Reason: `%s`") % (e)
        )

        return "" # If empty, the database won't get modified.
