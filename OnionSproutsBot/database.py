#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: Panagiotis "Ivory" Vasilopoulos <git@n0toose.net>
#
# :copyright:   (c) 2020-2022, Panagiotis "Ivory" Vasilopoulos
#
# :license: This is Free Software. See LICENSE for license information.
#

import sqlite3

def search_file_in_db(cursor: sqlite3.Cursor, filename: str):
    return cursor.execute(
        f'''SELECT binary, binary_id, sig, sig_id FROM tor_releases WHERE (
             binary = "{filename}"
        );'''
    ).fetchone()

def create_empty_db(cursor: sqlite3.Cursor, connection: sqlite3.Connection):
    cursor.execute(
        '''CREATE TABLE IF NOT EXISTS tor_releases (
            binary TEXT, binary_id INTEGER, sig TEXT, sig_id INTEGER,
            UNIQUE(binary, binary_id, sig, sig_id)
        );'''
    )

    return connection.commit()

def insert_new_release(
    cursor: sqlite3.Cursor,
    connection: sqlite3.Connection,
    name1: str,
    id1: int,
    name2: str,
    id2: int
):
    cursor.execute(
        f'''INSERT INTO tor_releases VALUES (
            '{name1}', '{id1}', '{name2}', '{id2}'
        );'''
    )

    return connection.commit()
