#!/usr/bin/env python3
#
# This file is part of OnionSpoutsBot, a Tor Browser distribution Telegram bot.
#
# :authors: Panagiotis "Ivory" Vasilopoulos <git@n0toose.net>
#           Evangelos "GeopJr" Paterakis <evan@geopjr.dev>
#
# :copyright:   (c) 2020-2022, Panagiotis "Ivory" Vasilopoulos
#
# :license: This is Free Software. See LICENSE for license information.
#

from distutils.util import get_platform
from . import database, files, titles, ui, helpers, i18n

from time import time
from datetime import datetime as dt
from pathlib import Path
from requests import HTTPError
from pyrogram import Client, filters
from pyrogram.types import (InlineQueryResultArticle, InputTextMessageContent,
                            InlineKeyboardMarkup, InlineKeyboardButton,
                            KeyboardButton, ReplyKeyboardMarkup)

import asyncio
import yaml
import logging
import os
import requests
import sqlite3
import sys
import urllib


# TODO: Better argument interface
with open(sys.argv[1], "r") as config:
    data = yaml.safe_load(config)

OnionSproutsBot = Client(
    "OnionSproutsBot",
    data['telegram']['api_id'],
    data['telegram']['api_hash'],
    bot_token=data['telegram']['bot_token']
)

i18n.setup_gettext()


@OnionSproutsBot.on_message(filters.command("start"))
async def start_command(client, message):
    user = message.from_user
    # User is not provided if the message is in a channel.
    if not user:
        return None
    user_lang = user.language_code.lower()
    # Set the default translation based on what Telegram returns.
    _ = i18n.get_translation(user_lang)

    lang_rows = []
    available_locales = i18n.available_locales.keys()
    # The sort function is being used to reorder the list so user.language_code
    # is the first button, in a way to make it easier for the user to pick
    # their language if Telegram returned the correct one.
    for lang in sorted(available_locales,key=user_lang.__eq__, reverse=True):
        lang_rows.append(InlineKeyboardButton(
            text=i18n.available_locales[lang]["full_name"],
            callback_data='welcome:' + lang
            )
        )

    button_rows = ui.get_rows(lang_rows, 4)

    lang_markup = InlineKeyboardMarkup(button_rows)

    await client.send_message(
        chat_id=message.chat.id,
        text=_("<strong>Language selection</strong>\n\n"
               "Please select your language:"
        ),
        reply_markup=lang_markup
    )

# Example query: welcome:en
@OnionSproutsBot.on_callback_query(filters.regex("^welcome:[^:]+$"))
async def welcome_command(client, callback):
    # All callbacks need to get the language from now on.
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_("<strong>Welcome!</strong>\n\n"
               "Hi, welcome to OnionSproutsBot! What would you like to do?"
        ),
        reply_markup=InlineKeyboardMarkup(
	    [
                [InlineKeyboardButton(
                    _("Download Tor Browser directly from Telegram"),
                    "request_tor:" + lang
                )],
                [InlineKeyboardButton(
                    _("Download Tor Browser from other mirrors"),
                    "request_tor_mirrors:" + lang
                )],
                [InlineKeyboardButton(
                    _("Get bridges to connect safely to Tor"),
                    "request_tor_bridges:" + lang
                )],
                [InlineKeyboardButton(
                    _("What is Tor?"),
                    "explain_tor:" + lang
                )]
            ]
        )
    )

    await client.answer_callback_query(
        callback.id
    )

# Example query: request_tor_mirrors:en
@OnionSproutsBot.on_callback_query(filters.regex("^request_tor_mirrors:[^:]+$"))
async def send_mirrors(client, callback):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text="<strong>Tor Browser Mirrors</strong>\n\n"
        "- Internet Archive: https://archive.org/details/tor_project_archives\n"
        "- GitLab: https://gitlab.com/thetorproject/torbrowser-version-arch/raw/master/\n"
        "- GitHub: https://github.com/torproject/torbrowser-releases/releases/download/torbrowser-release/\n"
        "- Email: gettor@torproject.org " + _("(Include your operating system: Windows, macOS, Linux)") + "\n\n" +
        _("(Make sure that your email provider is safe!)") + "\n"
    )

    await client.answer_callback_query(
        callback.id
    )

# Example query: request_tor_bridges:en
@OnionSproutsBot.on_callback_query(filters.regex("^request_tor_bridges:[^:]+$"))
async def send_mirrors(client, callback):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_("<strong>Tor Bridges</strong>\n\n"
               "Tor bridges are computers that can be very helpful when "
               "your internet provider has blocked you from using Tor, "
               "or if you just want to hide the fact that you are using Tor "
               "from anyone that could be monitoring your internet connection. "
               "(Such as your internet provider, government, household "
               "members, school, workplace, etc.)\n\n"
               "You can get a bridge from @getbridgesbot. "
               "You should then configure the Tor Browser to use it."
        ),
        reply_markup=InlineKeyboardMarkup(
        [
            [InlineKeyboardButton(
                _("Main Menu"),
                "welcome:" + lang
            )]
        ])
    )

    await client.answer_callback_query(
        callback.id
    )

# Example query: explain_tor:en
@OnionSproutsBot.on_callback_query(filters.regex("^explain_tor:[^:]+$"))
async def explain_tor(client, callback):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_("<strong>What is Tor?</strong>\n\n"
        "Tor is a network that lets you anonymously connect to the Internet. "
        "It's a kind of peer-to-peer network, in which people can connect to each other, "
        "without any central server or middleman.\n\n"
        "It can help you to connect to the Internet anonymously and without censorship."),
        reply_markup=InlineKeyboardMarkup(
        [
            [InlineKeyboardButton(
                _("Main Menu"),
                "welcome:" + lang
            )]
        ])
    )

    await client.answer_callback_query(
        callback.id
    )

# Example query: request_tor:en
@OnionSproutsBot.on_callback_query(filters.regex("^request_tor:[^:]+$"))
async def request_tor(client, callback):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    _ = i18n.get_translation(lang)

    platform_keyboard = []
    # Update check on new flow.
    response = await helpers.get_response(data['tor']['endpoint'])

    await client.send_message(
        chat_id=callback.from_user.id,
        text=_("<strong>Download Tor from Telegram</strong>\n\n"
            "Got it! I will have to ask you a couple of questions first."
        ),
    )

    # Generate list of available platforms, then put
    # each of them in separate buttons.
    for platform in ui.get_platform_list(response):
        platform_keyboard.append(InlineKeyboardButton(
            text=titles.platforms.get(platform.lower(), platform),
            # Here, "select_locale" may be a bit confusing,
            # but here, the buttons tell the bot to proceed
            # with select_locale, while telling it the platform that
            # was chosen.
            callback_data='select_locale:' + platform + ":" + lang
            )
        )

    # After we collect all of the buttons that we should we have,
    # we create our keyboard, and then send it to the user.
    button_rows = ui.get_rows(platform_keyboard, 2)
    platform_markup = InlineKeyboardMarkup(button_rows)

    await client.send_message(
        callback.from_user.id,
        _("Which operating system are you using?"),
        reply_markup=platform_markup
    )

    await client.answer_callback_query(
        callback.id
    )

# Example query: select_locale:linux64:en
@OnionSproutsBot.on_callback_query(filters.regex("^select_locale:[^:]+:[^:]+$"))
async def select_locale(client, callback):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    requested_platform = callback_data[1]
    _ = i18n.get_translation(lang)

    locale_keyboard = []

    # Make sure that the platform that was previously selected actually exists.
    platforms = ui.get_platform_list(response)
    if requested_platform.lower() not in platforms:
        return await client.answer_callback_query(callback.id)

    for locale in ui.get_locale_list(response, requested_platform):
        locale_keyboard.append(InlineKeyboardButton(
            text=titles.locales.get(locale.lower(), locale),
            callback_data='download_tor:' + requested_platform + ':' + locale + ":" + lang
            )
        )

    # Separates locales in rows of 4.
    button_rows = ui.get_rows(locale_keyboard, 4)

    locale_markup = InlineKeyboardMarkup(button_rows)
    await client.send_message(
        chat_id=callback.from_user.id,
        text=_("What is your preferred language?"),
        reply_markup=locale_markup
    )

    await client.answer_callback_query(
        callback.id
    )

# Example query: download_tor:linux64:it:en
@OnionSproutsBot.on_callback_query(filters.regex("^download_tor:[^:]+:[^:]+:[^:]+$"))
async def download_tor(client, callback):
    callback_data = callback.data.split(':')
    lang = callback_data[-1]
    platform = callback_data[1]
    locale = callback_data[2]
    _ = i18n.get_translation(lang)

    logging.info(f"Binary ({platform}, {locale}) requested.")

    # Checks if the requested platform and locale exist. If not, the
    # whole process is aborted for security reasons.
    if (platform not in ui.get_platform_list(response)) or \
            (locale not in ui.get_locale_list(response, platform)):
        logging.critical(f"Binary ({platform}, {locale}) NOT in list. Aborting...")
        return await client.answer_callback_query(callback.id)

    # Fetch the download link for the requested files with the response.
    # We do this regardless of whether we already have cached the files
    # or not, as we cannot easily predict the file name and submit a
    # request anyways.
    tor_sig_url = response['downloads'][platform][locale]['sig']
    tor_bin_url = response['downloads'][platform][locale]['binary']

    tor_sig_original_name = tor_sig_url.rsplit('/')[-1]
    tor_bin_original_name = tor_bin_url.rsplit('/', 1)[-1]

    # We check if the file has already been uploaded to Telegram. If so,
    # there should be an entry in our database for it.
    results = database.search_file_in_db(curs, tor_bin_original_name)

    if results != None:
        logging.info(f"Binary ({platform}, {locale}) found!")

        await client.send_cached_media(
            callback.from_user.id,
            file_id=results[1]
        )

        # Sends the cached version of the selected signature
        await client.send_cached_media(
            callback.from_user.id,
            file_id=results[3]
        )

        await client.send_message(
            chat_id=callback.from_user.id,
            text=_("<strong>Success!</strong>\n\n"
                   "We already had uploaded a copy of the version you "
                   "requested **(%s, %s)**, so we sent it to you instantly. "
                   "Stay safe!" % (platform, locale)
            ),
            reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton(
                    _("Main Menu"),
                    "welcome:" + lang
                )]
            ])
        )
    else:
        logging.info(f"Binary ({platform}, {locale}) not found.")

        await client.send_message(
            callback.from_user.id,
            _("The version you have requested **(%s, %s)** "
              "hasn't been uploaded to Telegram's servers yet. "
              "Please wait.") % (platform, locale)
        )

        await client.send_message(
            callback.from_user.id,
            _("Uploading signature file...")
        )

        tor_sig_id = await files.relay_files(
            client,
            callback,
            tor_sig_url,
            tor_sig_original_name,
            data['bot']['download_path'],
            _
        )

        await client.send_message(
            callback.from_user.id,
            _("Uploading the program...")
        )

        tor_bin_id = await files.relay_files(
            client,
            callback,
            tor_bin_url,
            tor_bin_original_name,
            data['bot']['download_path'],
            _
        )

        if len(tor_sig_id) == 0 or len(tor_bin_id) == 0:
            logging.info(f"Error during upload of file ({platform}, {locale}).")

            await client.send_message(
                chat_id=callback.from_user.id,
                text=_("<strong>Failure!</strong>\n\n"
                       "Something went wrong with the upload! "
                       "Please try again."
                ),
                reply_markup=InlineKeyboardMarkup(
                [
                    [InlineKeyboardButton(
                        _("Main Menu"),
                        "welcome:" + lang
                    )]
                ])
            )

            return
        else:
            database.insert_new_release(
                curs,
                conn,
                tor_bin_original_name,
                tor_bin_id,
                tor_sig_original_name,
                tor_sig_id
            )

            conn.commit()

            await client.send_message(
                chat_id=callback.from_user.id,
                text=_("<strong>Success!</strong>\n\n"
                       "The version you have requested **(%s, %s)** has been "
                       "successfully uploaded and sent to you. "
                       "Stay safe!" % (platform, locale)
                ),
                reply_markup=InlineKeyboardMarkup(
                [
                    [InlineKeyboardButton(
                        _("Main Menu"),
                        "welcome:" + lang
                    )]
                ])
            )

            logging.info(f"--- NEW ENTRY ---")
            logging.info(f"Binary name: {tor_bin_original_name}: ")
            logging.info(f"Binary cache ID: {tor_bin_id}: ")
            logging.info(f"Signature name: {tor_sig_original_name}:")
            logging.info(f"Signature cache ID: {tor_sig_id}")
            logging.info(f"-----------------")

    await client.answer_callback_query(
        callback.id
    )

def init():
    global conn, curs, response

    '''
    We take care of all of the logging stuff before we
    # do anything else. There definitely has to be some sort
    # of a filename though, so we hardcode "OSB" in case
    # nothing else is available.
    '''

    # We start off with the path where files will be stored.
    # If one hasn't been defined, it will be left blank and
    # the default directory will be the current working one.
    log_filename = str(data['logging']['directory'] or '')

    # Carrying on with the file name...
    if (data['logging']['filename_suffix'] is not None):
        log_filename += data['logging']['filename_suffix']
    else:
        log_filename += "OSB"

    # We concatenate the date and the time for better organization...
    # As always, this is optional, but configured by default.
    if (data['logging']['filename_datefmt'] is not None):
        # Adds separator between name and date, if defined.
        log_filename += str(data['logging']['date_separator'] or '')
        # Adds date depending on format defined in the configuration.
        log_filename += dt.now().strftime(data['logging']['filename_datefmt'])

    logging.basicConfig(
        filename=f'{log_filename}.log',
        format=data['logging']['format'],
        encoding=data['logging']['encoding'],
        level=logging.INFO,
        datefmt=f"{data['logging']['datefmt']}"
    )

    logging.getLogger().addHandler(logging.StreamHandler()) # prints to stderr

    # Okay, here comes the real deal.
    logging.info("=== OnionSproutsBot ===")

    # Custom paths
    if (data['bot']['download_path'] is not None):
        logging.info(f"Custom download path: {data['bot']['download_path']}")

    try:
        if (data['bot']['db_path'] is None):
            logging.info(f"Database path: {os.getcwd()}/{data['defaults']['db_name']}")
            conn = sqlite3.connect(f"{os.getcwd()}/{data['defaults']['db_name']}")
        else:
            conn = sqlite3.connect(data['bot']['db_path'])
    except Exception as e:
        logging.critical(e)
        exit(1)

    curs = conn.cursor()

    database.create_empty_db(curs, conn)

    # Obtains a list of available downloads on startup.
    # Don't worry about updates, it gets updated after
    # a request is made. This variable will be overwritten
    # by a function that achieves the same goal, but does so
    # asynchronously, later on.
    response = requests.get(data['tor']['endpoint']).json()
    logging.info("Initial response from endpoint obtained.")

    conn.commit()
    logging.info("Connection to database initiated.")
    OnionSproutsBot.run()
    conn.close()
